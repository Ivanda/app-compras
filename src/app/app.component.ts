import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: "AIzaSyC9Nys2vE7-eBrIlnIQ9LhHM7twpMIvTG8",
      authDomain: "comprasapp-e29df.firebaseapp.com"
    });
  }

}
