import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PresupuestosService {

  presURL = "https://comprasapp-e29df.firebaseio.com/presupuestos.json";
  preURL = "https://comprasapp-e29df.firebaseio.com/presupuestos";

  constructor(private http: HttpClient) { }

  postPresupuesto(presupuesto: any) {
    const newpres = JSON.stringify(presupuesto);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post(this.presURL, newpres, {headers}).pipe(res => {
      return res;
    })
  }

  getPresupuestos() {
    return this.http.get(this.presURL).pipe(res => {
      return res;
    })
  }

  getPresupuesto(id$: string) {
    const url = `${this.preURL}/${id$}.json`;
    return this.http.get(url).pipe(res => res);
  }

  putPresupuesto(presupuesto: any, id$: string) {
    const newpre = JSON.stringify(presupuesto);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    const url = `${this.preURL}/${id$}.json`;
    return this.http.put(url, newpre, {headers}).pipe(res => res);
  }

  delPresupuesto(id$: string) {
    const url = `${this.preURL}/${id$}.json`;
    return this.http.delete(url).pipe(res => res);
  }
}
