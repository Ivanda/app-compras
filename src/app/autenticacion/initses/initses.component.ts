import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AutenticacionService } from '../../servicios/autenticacion.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-initses',
  templateUrl: './initses.component.html',
  styleUrls: ['./initses.component.css']
})
export class InitsesComponent implements OnInit {

  loginForm: FormGroup;
  userData: any;

  mensaje = false;

  constructor(private formBuilder: FormBuilder, private autenticacionService: AutenticacionService, private router: Router, private activatedRouter: ActivatedRoute) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      'email': ['', [Validators.email, Validators.required]],
      'password': ['', [Validators.required, Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'), Validators.minLength(6)]]
    });
  }

  onSubmit() {
    this.userData = this.saveUserData();
    this.autenticacionService.inicioSesion(this.userData);
    setTimeout(() => {
      if(this.isAuth() === false) {
        this.mensaje = true;
      }
    }, 2000);
  }

  saveUserData() {
    const saveUserData = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    }

    return saveUserData;
  }

  isAuth() {
    return this.autenticacionService.isAuthenticated();
  }

}
